﻿using System.Diagnostics;
using Microsoft.Data.SqlClient;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Channels;
using MarketPlace;
using MarketPlace.Frame;
using StackExchange.Redis;


var appH = new ApplicationHelper();
await appH.AddMiddleware(new RouterMiddleware()).UseStaticFiles().AddMiddleware(new TerminalMiddleware()).Run();