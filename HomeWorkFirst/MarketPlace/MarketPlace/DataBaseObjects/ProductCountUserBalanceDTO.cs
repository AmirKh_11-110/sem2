
namespace MarketPlace;

public class ProductCountUserBalanceDTO
{
    public long ProductCount { get; set; }
    public long Balance { get; set; }
    
}