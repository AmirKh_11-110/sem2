namespace MarketPlace;

public class ReviewDTO
{
    public string Name { get; set; }
    public string Message { get; set; }
    public int Rating { get; set; }
    public int Id { get; set; }
    public int Price { get; set; }
}