﻿namespace MarketPlace;

public class DeletingReview
{
    public string Message { get; set; }
    public int Rating { get; set; }
    public int ProductId { get; set; }
}