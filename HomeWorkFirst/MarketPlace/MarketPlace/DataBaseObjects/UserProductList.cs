using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketPlace
{
    public class UserProductList
    {
        public string Name { get; set; }
        public decimal Count { get; set; }
        public decimal Price { get; set; }
    }
}