namespace MarketPlace;

public class UpdatingPasswordDTO
{
    public  string LastPassword{ get; set; }
    public  string NewPassword{ get; set; }
}
