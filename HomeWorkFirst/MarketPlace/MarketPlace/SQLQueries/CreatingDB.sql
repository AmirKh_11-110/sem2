CREATE DATABASE marketplace
    WITH
    OWNER = omr
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;