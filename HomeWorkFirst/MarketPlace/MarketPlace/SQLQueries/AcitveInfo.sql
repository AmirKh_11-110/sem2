CREATE TABLE public."ActiveInfo"
(
    enter_date date,
    leave_date date,
    user_id integer
);

ALTER TABLE IF EXISTS public."ActiveInfo"
    OWNER to omr;