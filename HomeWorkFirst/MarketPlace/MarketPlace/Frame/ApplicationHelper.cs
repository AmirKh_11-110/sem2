using System.Net;
using System.Reflection;
using System.Text;

namespace MarketPlace.Frame;

public class ApplicationHelper
{
    public IMiddleware FirstMiddleware { get; set; }
    public IMiddleware CurrentMiddleware { get; set; }
    public async Task Run()
    {
        //await AddMiddleware(new TerminalMiddleware());
        HttpListener listener = new();
        listener.Prefixes.Add("http://localhost:1111/");
        listener.Start();
        while (listener.IsListening)
        {
            FirstMiddleware.Invoke(await listener.GetContextAsync());
        }
    }

    public ApplicationHelper AddMiddleware(IMiddleware middleware)
    {
        if (FirstMiddleware is null)
        {
            FirstMiddleware = middleware;
            CurrentMiddleware = FirstMiddleware;
        }
        else if (CurrentMiddleware.GetType().GetProperty("_next", BindingFlags.Instance | BindingFlags.NonPublic) is not null)
        {
            CurrentMiddleware.GetType().
                GetProperty("_next", BindingFlags.Instance | BindingFlags.NonPublic)?.
                SetValue(CurrentMiddleware,middleware.Invoke);
            CurrentMiddleware = middleware;
        }
        else
        {
            throw new Exception("Нельзя добавить middleware без свойтва \"_next\"");
        }

        return this;
    }
}

public static class ExtensionHelper
{
    public static ApplicationHelper UseStaticFiles(this ApplicationHelper appH)
    {
        appH.AddMiddleware(new StaticFilesMiddleware());
        return appH;
    }
}

public interface IMiddleware
{
    public void Invoke(HttpListenerContext context);
}

public class StaticFilesMiddleware : IMiddleware
{
    private Action<HttpListenerContext> _next { get; set; }

    public async void Invoke(HttpListenerContext context)
    {
        var response = context.Response;
        var requestPath = context.Request.RawUrl;
        var staticPath = Path.Combine(Directory.GetCurrentDirectory(), $"WWW{requestPath}");
        Console.WriteLine(File.Exists(staticPath) + " " + staticPath);
        if (File.Exists(staticPath))
        {
            await ShowFile(response, staticPath);
        }
        else
        {
            await ShowError(response);
        }
        _next.Invoke(context);
    }

    public static async Task ShowFile(HttpListenerResponse response, string path)
    {
        response.ContentType = Path.GetExtension(path) switch
        {
            ".js" => "application/javascript",
            ".html" => "text/html",
            ".css" => "text/css",
            ".png" => "image/png",
            ".cur" => "image/cur",
            ".ani" => "image/ani",
            ".gif" => "image/gif",
            _ => throw new NotImplementedException()
        };
        response.StatusCode = 200;
        using var stream = response.OutputStream;
        await stream.WriteAsync(File.ReadAllBytes(path));
    }

    public static async Task ShowError(HttpListenerResponse response)
    {
        response.ContentType = "text/plain";
        response.StatusCode = 404;
        using var stream = response.OutputStream;
        await stream.WriteAsync(Encoding.UTF8.GetBytes("Error!!!"));
    }
}

public class TerminalMiddleware : IMiddleware
{
    public void Invoke(HttpListenerContext context)
    {
        
    }
}

public class RouterMiddleware : IMiddleware
{
    private Action<HttpListenerContext> _next { get; set; }
    public async void Invoke(HttpListenerContext context)
    {
        switch (context.Request.Url?.LocalPath)
        {
            //Pages
            case "/":
                await WebHelper.Home(context);
                break;
            case "/productsNotRegistered":
                await WebHelper.ProductsNotRegistered(context);
                break;
            case "/signIn":
                await WebHelper.SignIn(context);
                break;
        }
        _next.Invoke(context);
    }
}