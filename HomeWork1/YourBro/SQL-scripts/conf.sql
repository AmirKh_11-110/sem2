create table public.users
(
    user_id   integer not null,
    user_name text    not null,
    user_password  text    not null
);

comment on table public.users is 'users_table';

create unique index users_user_id_uindex
    on public.users (user_id);

alter table public.users
    add constraint users_pk
        primary key (user_id);
