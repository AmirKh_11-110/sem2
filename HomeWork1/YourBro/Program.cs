using YourBro.Services.Abstractions;
using YourBro.Services.Implementations;
using Infrastructure;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
var connection = builder.Configuration.GetConnectionString("ApplicationContext");
builder.Services.AddDbContext<ApplicationContext>(options => options.UseNpgsql(connection));

builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IAdviceService, AdviceService>();
builder.Services.AddScoped<IUsersRatingService, UsersRatingService>();
var serviceProvider = builder.Services.BuildServiceProvider();
var context = serviceProvider.GetService<ApplicationContext>();

///
//var a = builder.Services.BuildServiceProvider().GetRequiredService<ApplicationContext>();
// Add services to the container.
builder.Services.AddRazorPages();

var app = builder.Build();

if (context.Database.CanConnect())
{
    Console.WriteLine("Database connection successful!");
}
else
{
    Console.WriteLine("Database connection failed!");
}

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.Map("/info", appBuilder =>
{
    appBuilder.Run(async context =>
    {
        context.Response.ContentType = "text/html";
        await context.Response.SendFileAsync("wwwroot/index.html");
    });
});

//app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();
app.Use(async (context, next) =>
{
    var date = DateTime.Now.ToShortTimeString();
    await next.Invoke();
    Console.WriteLine($"Время: {date}");
});



app.Run();