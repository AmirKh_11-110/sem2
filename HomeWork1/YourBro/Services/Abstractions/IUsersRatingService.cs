using Domain.Models;

namespace YourBro.Services.Abstractions;

public interface IUsersRatingService
{
    Task AddUsersRatingAsync(UsersRating usersRating);
    Task<UsersRating> GetUsersRatingAsync(UsersRating usersRating);
    Task UpdateUsersRatingAsync(UsersRating usersRating);
    Task DeleteUsersRatingAsync(UsersRating usersRating);
}