using StackExchange.Redis;

namespace YourBro.Services.Implementations;

public class SessionService
{
    private readonly IDatabase _database;
    private readonly string _prefix = "Session_";
    private readonly int _expireTimeSeconds = 30 * 60;

    public SessionService(ConnectionMultiplexer redis)
    {
        _database = redis.GetDatabase();
    }

    public string CreateSession(int userId)
    {
        string sessionId = Guid.NewGuid().ToString();
        _database.StringSet(_prefix + sessionId, userId, TimeSpan.FromSeconds(_expireTimeSeconds));
        return sessionId;
    }

    public bool CheckSession(string sessionId, out int userId)
    {
        userId = -1;
        RedisValue sessionValue = _database.StringGet(_prefix + sessionId);
        if (sessionValue.IsNullOrEmpty)
        {
            return false;
        }

        userId = (int) sessionValue;
        _database.KeyExpire(_prefix + sessionId, TimeSpan.FromSeconds(_expireTimeSeconds));
        return true;
    }

    public void RemoveSession(string sessionId)
    {
        _database.KeyDelete(_prefix + sessionId);
    }
}
