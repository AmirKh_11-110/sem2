using Domain.Models;

namespace YourBro.Services.Abstractions;

public interface IUserService
{
    Task AddUserAsync(User user);
    Task<User>  GetUserAsync(User user);
    Task UpdateUserAsync(User user);
    Task DeleteUserAsync(User user);
}