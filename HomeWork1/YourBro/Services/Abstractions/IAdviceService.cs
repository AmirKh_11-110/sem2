using Domain.Models;

namespace YourBro.Services.Abstractions;

public interface IAdviceService
{
    Task AddAdvice(Advice advice);
    Task GetAdvice(Advice advice);
    Task UpdateAdvice(Advice advice);
    Task DeleteUAdvice(Advice advice);
}