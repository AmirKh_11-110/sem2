using Domain.Models;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using YourBro.Services.Abstractions;

namespace YourBro.Services.Implementations;

public class UsersRatingService : IUsersRatingService
{
    private readonly ApplicationContext _context;

    public UsersRatingService(ApplicationContext context)
    {
        _context = context;
    }

    public async Task AddUsersRatingAsync(UsersRating rating)
    {
        await _context.UsersRatings.AddAsync(rating);
        await _context.SaveChangesAsync();
    }

    public async Task DeleteUsersRatingAsync(UsersRating rating)
    {
        _context.UsersRatings.Remove(rating);
        await _context.SaveChangesAsync();
    }

    public async Task<UsersRating> GetUsersRatingAsync(UsersRating rating)
    {
        return await _context.UsersRatings.FirstOrDefaultAsync(r => r.Id == rating.Id);
    }

    public async Task UpdateUsersRatingAsync(UsersRating rating)
    {
        _context.UsersRatings.Update(rating);
        await _context.SaveChangesAsync();
    }
}