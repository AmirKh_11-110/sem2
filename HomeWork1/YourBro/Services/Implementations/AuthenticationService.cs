using System.Text;
using Microsoft.EntityFrameworkCore.Storage;
using StackExchange.Redis;

namespace YourBro.Services.Abstractions;

public class RedisAuthService
{
    private readonly ConnectionMultiplexer _redis;
    private readonly StackExchange.Redis.IDatabase _database;

    public RedisAuthService(string connectionString)
    {
        _redis = ConnectionMultiplexer.Connect(connectionString);
        _database = _redis.GetDatabase();
    }

    public bool CheckAuthenticate(string username, string password)
    {
        var userHash = GetHash(username);

        if (userHash == null || userHash.PasswordHash != CalculatePasswordHash(password))
        {
            return false;
        }

        return true;
    }

    public void Register(int id, string username, string password)
    {
        var passwordHash = CalculatePasswordHash(password);

        var hashEntry = new HashEntry[]
        {
            new HashEntry("Username", username),
            new HashEntry("PasswordHash", passwordHash),
        };

        _database.HashSet(username, hashEntry);
    }

    private UserHash GetHash(string username)
    {
        var hashEntry = _database.HashGetAll(username);
        if (hashEntry.Length == 0)
        {
            return null;
        }

        var userHash = new UserHash();
        foreach (var entry in hashEntry)
        {
            if (entry.Name == "Username")
            {
                userHash.Username = entry.Value.ToString();
            }
            else if (entry.Name == "PasswordHash")
            {
                userHash.PasswordHash = entry.Value.ToString();
            }
        }

        return userHash;
    }

    private string CalculatePasswordHash(string password)
    {
        string hashString;
        using (var sha512 = System.Security.Cryptography.SHA512.Create())
        {
            var hash = sha512.ComputeHash(Encoding.UTF8.GetBytes(password));
            hashString = Convert.ToBase64String(hash);
        }

        return hashString;
    }
}

public class UserHash
{
    public string Username { get; set; }
    public string PasswordHash { get; set; }
}