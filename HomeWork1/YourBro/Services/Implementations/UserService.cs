using Domain.Models;
using YourBro.Services.Abstractions;
using Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace YourBro.Services.Implementations
{
    public class UserService : IUserService
    {
        private readonly ApplicationContext _applicationContext;

        public UserService(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public async Task AddUserAsync(User user)
        {
            await _applicationContext.Users.AddAsync(user);
            await _applicationContext.SaveChangesAsync(); // сохраняем изменения в базе данных
        }
        
        public async Task<User> GetUserAsync(User user)
        {
           return await _applicationContext.Users.FirstOrDefaultAsync(x => x.Equals(user));
        }
        
        public async Task<User?> GetUserAsync(int id)
        {
            // используем метод Include для загрузки связанных сущностей (в данном случае, рейтинга пользователя)
            var user = await _applicationContext.Users
                .Include(u => u.Rating)
                .FirstOrDefaultAsync(u => u.Id == id);

            return user;
        }

        public async Task UpdateUserAsync(User user)
        {
            _applicationContext.Users.Update(user);
            await _applicationContext.SaveChangesAsync();
        }

        public async Task DeleteUserAsync(User user)
        {
            _applicationContext.Users.Remove(user);
            await _applicationContext.SaveChangesAsync();
        }
    }
}
