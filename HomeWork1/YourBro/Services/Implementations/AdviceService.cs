using Domain.Models;
using YourBro.Services.Abstractions;
using Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace YourBro.Services.Implementations;

public class AdviceService : IAdviceService
{
    private ApplicationContext _applicationContext { get; }
    public async Task AddAdvice(Advice advice)
    {
        await _applicationContext.Advices.AddAsync(advice);
    }

    public async Task GetAdvice(Advice advice)
    {
        await _applicationContext.Advices.FirstOrDefaultAsync(x => x.Equals(advice));
    }

    public async Task UpdateAdvice(Advice advice)
    {
        _applicationContext.Advices.Update(advice);
        await _applicationContext.SaveChangesAsync();
    }

    public async Task DeleteUAdvice(Advice advice)
    {
        _applicationContext.Advices.Remove(advice);
        await _applicationContext.SaveChangesAsync();
    }
}