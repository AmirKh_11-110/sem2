﻿using Microsoft.AspNetCore.Mvc.RazorPages;

namespace HomeWork1.Pages;

public class IndexModel : PageModel
{
    private readonly ILogger<IndexModel> _logger;

    public IndexModel(ILogger<IndexModel> logger)
    {
        _logger = logger;
    }

    public string Message { get; set; }
    public string LastRequest { get; set; }
    
    public void OnGet()
    {
        Message = "Приветик!";
    }

    public void OnPost(string mess)
    {
        LastRequest = mess;
        if (mess is null)
        {
            Message = "Введи что-нибудь!";
        }
        else
        {
            Message = "Молодец!";
        }
    }
}