using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure;

public class ApplicationContext : DbContext
{
    public DbSet<User> Users { get; set; } = null!;
    public DbSet<Advice> Advices { get; set; } = null!;
    public DbSet<UsersRating> UsersRatings { get; set; } = null!;

    public ApplicationContext(DbContextOptions<ApplicationContext> options)
        : base(options)
    {
        Database.EnsureCreated();   // создаем базу данных при первом обращении
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>().HasData(
            new User { Id = 1, Name = "Tom", Password = "1234" },
            new User { Id = 2, Name = "Bob", Password = "1234" },
            new User { Id = 3, Name = "Sam", Password = "1234" }
        );
        
        // связь многие-ко-многим между Users и UsersRatings
        modelBuilder.Entity<UsersRating>()
            .HasKey(ur => new { ur.UserSenderId, ur.UserReceiverId });

        modelBuilder.Entity<UsersRating>()
            .HasOne(ur => ur.UserSender)
            .WithMany(u => u.Rating)
            .HasForeignKey(ur => ur.UserSenderId)
            .OnDelete(DeleteBehavior.Restrict);

        modelBuilder.Entity<UsersRating>()
            .HasOne(ur => ur.UserReceiver)
            .WithMany()
            .HasForeignKey(ur => ur.UserReceiverId)
            .OnDelete(DeleteBehavior.Restrict);

        // связь один-ко-многим между User и Advice
        modelBuilder.Entity<Advice>()
            .HasOne(a => a.User)
            .WithMany()
            .HasForeignKey(a => a.UserId);
    }
}