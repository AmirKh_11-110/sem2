using System.Threading.Tasks;
using Domain.Models;
using YourBro.Services.Implementations;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace TestServices
{
    [TestFixture]
    public class UserRatingTests
    {
        private UserService _userService;
        private UsersRatingService _userRatingService;
        private DbContextOptions<ApplicationContext> _options;
        private User _user;

        [SetUp]
        public void Setup()
        {
            _options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseNpgsql("Server=localhost;Port=5432;Database=pgDB;User Id=omr;Password=1234;")
                .Options;

            _user = new User
            {
                Name = "TestUser",
                Password = "TestPassword"
            };
            _userService = new UserService(new ApplicationContext(_options));
            _userRatingService = new UsersRatingService(new ApplicationContext(_options));
        }

        [Test]
        public async Task AddRatingAsync_AddsRatingToUser()
        {
            // Arrange
            await _userService.AddUserAsync(_user);

            var rating = new UsersRating
            {
                Rating = 5,
                UserSenderId = _user.Id,
                UserReceiverId = _user.Id
            };

            // Act
            await _userRatingService.AddUsersRatingAsync(rating);

            // Assert
            var savedRating = await _userRatingService.GetUsersRatingAsync(rating);
            Assert.IsNotNull(savedRating);
            Assert.AreEqual(rating.Rating, savedRating.Rating);
            Assert.AreEqual(rating.UserSenderId, savedRating.UserSenderId);
            Assert.AreEqual(rating.UserReceiverId, savedRating.UserReceiverId);
        }

        [TearDown]
        public void TearDown()
        {
            using (var context = new ApplicationContext(_options))
            {
                context.Database.EnsureDeleted();
            }
        }
    }
}
