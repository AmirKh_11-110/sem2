using System;
using System.Linq;
using System.Threading.Tasks;
using Domain.Models;
using YourBro.Services.Implementations;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace TestServices
{
    [TestFixture]
    public partial class ServiceTest
    {
        private UserService _userService { get; set; }
        private DbContextOptions<ApplicationContext> _options;

        [SetUp]
        public void Setup()
        {
            _options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseNpgsql("Server=localhost;Port=5432;Database=pgDB;User Id=omr;Password=1234;")
                .Options;
        }


        [Test]
        public void TestConnection()
        {
            using (var context = new ApplicationContext(_options))
            {
                // тест подключения к базе данных
                Assert.IsTrue(context.Database.CanConnect());
                _userService = new UserService(context);
            }
        }

        [TestFixture]
        public class UserTests
        {
            private ApplicationContext _context;
            private DbContextOptions<ApplicationContext> _options;
            private UserService _us { get; set; }

            [SetUp]
            public void Setup()
            {
                _options = new DbContextOptionsBuilder<ApplicationContext>()
                    .UseNpgsql("Server=localhost;Port=5432;Database=pgDB;User Id=omr;Password=1234;")
                    .Options;
                _context = new ApplicationContext(_options);
                _us = new UserService(_context);
            }

            [Test]
            public async Task AddUser_ToDatabase()
            {
                // Arrange
                var user = new User
                {
                    Name = "SOMENAMEISNOTEMTY",
                    Password = "REaalyHartTOGessIT252654@#4lk4356LEtsSGooo"
                };

                // Act
                await _us.AddUserAsync(user);
                await _context.SaveChangesAsync();

                // Assert
                var savedUser = await _context.Users.FindAsync(user.Id);
                Assert.IsNotNull(savedUser);
                Assert.AreEqual(user.Name, savedUser.Name);
                Assert.AreEqual(user.Password, savedUser.Password);
            }

            [Test]
            public async Task Fail_AddUser_AddsUserToDatabase()
            {
                // Arrange
                var user = new User
                {
                    Name = "SOMENAMEISNOTEMTYTwo",
                    Password = "REaalyHartTOGessIT252654@#4lk4356LEtsSGoooTwo",
                    Id = 1
                };

                // Act
                try
                {
                    await _us.AddUserAsync(user);
                    await _context.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    // ignored
                }

                // Assert
                var savedUser = await _context.Users.SingleOrDefaultAsync(u =>
                    u.Name == "SOMENAMEISNOTEMTYTwo" && u.Password == "REaalyHartTOGessIT252654@#4lk4356LEtsSGoooTwo" &&
                    u.Id == 1);
                Assert.IsNull(savedUser);
            }

            
            [Test]
            public async Task DeleteUser_RemovesUserFromDatabase()
            {
                // Arrange
                _options = new DbContextOptionsBuilder<ApplicationContext>()
                    .UseNpgsql("Server=localhost;Port=5432;Database=pgDB;User Id=omr;Password=1234;")
                    .Options;
                var test_User = new User { Name = "TestUser404", Password = "L:JDSFPOWEIF89OWEFI(89349ui&*(WE$f" };
                using (var context = new ApplicationContext(_options))
                {
                    context.Users.Add(test_User);
                    context.SaveChanges();
                }

                using (var context = new ApplicationContext(_options))
                {

                    // Act
                    var user = await context.Users.SingleOrDefaultAsync(u => u.Name == test_User.Name && test_User.Password == u.Password);
                    _us.DeleteUserAsync(user);
                    context.SaveChanges();

                    // Assert
                    var deletedUser = _us.AddUserAsync(test_User);
                    Assert.IsNotNull(deletedUser);
                }
            }
            
            [TearDown]
            public void TearDown()
            {
                _context.Database.EnsureDeleted();
                _context.Dispose();
            }
        }
    }
}  