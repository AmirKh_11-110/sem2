using System;
using System.Linq;
using Domain.Models;
using Infrastructure;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace TestServices;

[TestFixture]
public partial class ServiceTest 
{
    [TestFixture]
    public class AdviceTests
    {
        private ApplicationContext _context;
        private DbContextOptions<ApplicationContext> _options;

        [SetUp]
        public void Setup()
        {
            _options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseNpgsql("Server=localhost;Port=5432;Database=pgDB;User Id=omr;Password=1234;")
                .Options;


            _context = new ApplicationContext(_options);
        }

        [Test]
        public void AddAdvice_ToDatabase()
        {
            // Arrange
            var user = new User
            {
                Name = "SOMENAMEISNOTEMTY3",
                Password = "REaalyHartTOGessIT252654@#4lk4356LEtsSGooo3"
            };
            var advice = new Advice
            {
                User = user,
                Text = "advice"
            };

            // Act
            _context.Advices.Add(advice);
            _context.SaveChanges();

            // Assert
            var savedAdvice = _context.Advices.SingleOrDefault(a => a.User.Name == "SOMENAMEISNOTEMTY3" && a.User.Password == "REaalyHartTOGessIT252654@#4lk4356LEtsSGooo3" && a.Text == "advice");
            Assert.IsNotNull(savedAdvice);
            Assert.AreEqual(advice.Text, savedAdvice.Text);
        }
        [TearDown]
        public void TearDown()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}