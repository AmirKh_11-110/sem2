namespace Domain.Models;

public class UsersRating
{
    public int Id { get; set; }
    public int Rating { get; set; }
    public int UserSenderId { get; set; }
    public int UserReceiverId { get; set; }
    public User UserSender { get; set; }
    public User UserReceiver { get; set; }
}